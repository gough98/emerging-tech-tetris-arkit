//
//  TetrisShapes.swift
//  Emerging Tech Tetris
//
//  Created by Cameron Gough on 19/11/2018.
//  Copyright © 2018 Cameron Gough. All rights reserved.
//
class OneSidedTetrisShape {
    
    static let all = [a, b, c, b, e, f, g]
    
    static let a = OneSidedTetrisShape([
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 2, 0]),
        FixedTetrisShape([1, 0, 1, 1, 1, 2, 2, 2]),
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 0, 2]),
        FixedTetrisShape([1, 0, 1, 1, 1, 2, 0, 0])])
    
    static let b = OneSidedTetrisShape (
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 3, 1]),
        FixedTetrisShape([1, 0, 1, 1, 1, 2, 1, 3])
    )
    
    static let c = OneSidedTetrisShape([
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 2, 2]),
        FixedTetrisShape([1, 0, 1, 1, 1, 2, 0, 2]),
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 0, 0]),
        FixedTetrisShape([1, 0, 1, 1, 1, 2, 2, 0])]
    )
    
    static let d = OneSidedTetrisShape([
        FixedTetrisShape([1, 0, 0, 1, 1, 1, 2, 1]),
        FixedTetrisShape([1, 0, 0, 1, 1, 1, 1, 2]),
        FixedTetrisShape([0, 1, 1, 1, 2, 1, 1, 2]),
        FixedTetrisShape([1, 0, 1, 1, 2, 1, 1, 2])]
    )
    
    static let e = OneSidedTetrisShape(
        FixedTetrisShape([1, 0, 2, 0, 0, 1, 1, 1]),
        FixedTetrisShape([0, 0, 0, 1, 1, 1, 1, 2])
    )
    
    static let f = OneSidedTetrisShape(
        FixedTetrisShape([0, 0, 0, 1, 1, 0, 1, 1])
    )
    
    static let g = OneSidedTetrisShape(
        FixedTetrisShape([0, 0, 1, 0, 1, 1, 2, 1]),
        FixedTetrisShape([1, 0, 0, 1, 1, 1, 0, 2])
    )
    
    let fixed: [FixedTetrisShape]
    
    init(_ t: FixedTetrisShape) { self.fixed = [t, t, t, t] }
    
    init(_ t1: FixedTetrisShape, _ t2: FixedTetrisShape) { self.fixed = [t1, t2, t1, t2] }
    
    init(_ fixed: [FixedTetrisShape]) { self.fixed = fixed }
    }

class FixedTetrisShape{
    
    let values: [Int]
    
    init(_ values: [Int]) { self.values = values }
    
    func x(_ index: Int) -> Int { return values[index * 2] }
    
    func y(_ index: Int) -> Int { return values[index * 2 + 1] }
    
}
