//
//  TetrisGrid.swift
//  Emerging Tech Tetris
//
//  Created by Cameron Gough on 19/11/2018.
//  Copyright © 2018 Cameron Gough. All rights reserved.
//

// this sets up the grid that the tetris will be played in

import Foundation
import SceneKit

class TetrisGrid {
    
    private let config: TetrisSettings
    private var matrix: [[Bool]] = []
    
    init(_ config: TetrisSettings) {
        self.config = config
        matrix.append([Bool](repeating: true, count: config.width + 2))
        for _ in 0...config.height + 3 {
            addLine()
        }
    }
    
    public func hasCollision(_ Position: TetrisPosition) -> Bool {
        let tetrisShape = Position.tetrisShape()
        for i in 0...3 {
            if (matrix[Position.y + tetrisShape.y(i)][Position.x + tetrisShape.x(i)]) {
                return true
            }
        }
        return false
    }
    
    public func add(_ current: TetrisPosition) {
        let tetrisShape = current.tetrisShape()
        for i in 0...3 {
            matrix[current.y + tetrisShape.y(i)][current.x + tetrisShape.x(i)] = true
        }
    }
    
    public func clearFilledLines() -> [Int] {
        var toRemove: [Int] = []
        loop: for i in 1...config.height + 1 {
            for j in 1...config.width {
                if (!matrix[i][j]) {
                    continue loop
                }
            }
            toRemove.append(i)
        }
        toRemove.reverse()
        for i in toRemove {
            matrix.remove(at: i)
            addLine()
        }
        return toRemove
    }
    
    private func addLine() {
        var row = [Bool](repeating: false, count: config.width + 2)
        row[0] = true
        row[config.width + 1] = true
        matrix.append(row)
    }
    
    
    
}

//this is the struct for the grid size
struct  TetrisSettings {
    static let standard: TetrisSettings = TetrisSettings(width: 10, height: 20)
    
    let width: Int
    let height: Int
}

class TetrisPosition {
    
    static func random(_ settings: TetrisSettings) -> TetrisPosition {
        return TetrisPosition(random(OneSidedTetrisShape.all.count), random(4), settings.width / 2, settings.height)
    }
    
    let index: Int
    let rotation: Int
    let x: Int
    let y: Int
    
    private init(_ index: Int, _ rotation: Int, _ x: Int, _ y: Int) {
        self.index = index
        self.rotation = rotation
        self.x = x
        self.y = y
    }
    
    func tetrisShape() -> FixedTetrisShape { return OneSidedTetrisShape.all[index].fixed[rotation] }
    
    func rotate() -> TetrisPosition { return TetrisPosition(index, (rotation + 1) % 4, x, y) }
    
    func left() -> TetrisPosition { return TetrisPosition(index, rotation, x - 1, y) }
    
    func right() -> TetrisPosition { return TetrisPosition(index, rotation, x + 1, y) }
    
    func down() -> TetrisPosition { return TetrisPosition(index, rotation, x, y - 1) }
    
    private static func random(_ max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(max)))
    }
    
}

