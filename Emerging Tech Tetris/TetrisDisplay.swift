//
//  TetrisDisplay.swift
//  Emerging Tech Tetris
//
//  Created by Cameron Gough on 19/11/2018.
//  Copyright © 2018 Cameron Gough. All rights reserved.
//


import Foundation
import SceneKit

class TetrisDisplay {
    
    private var blocksByLine: [[SCNNode]] = []
    private var recent: SCNNode!
    private var frame: SCNNode!
    
    private let cell: Float = 0.05
    
    private let settings: TetrisSettings
    private let display: SCNScene
    private let x: Float
    private let y: Float
    private let z: Float
    
    
    // define colours for the grid, floor, score and title
    private static let scoresColour = UIColor(red:0.50, green:1.0, blue:0.5, alpha:1.0)
    private static let gridColour = UIColor(red:0, green:0, blue:1.0, alpha:1.0)
    private static let titleColour = UIColor(red:0.50, green:0.45, blue:0.75, alpha:1.0)
    private static let floorColour = UIColor(red:0, green:0, blue:0, alpha:0.3)
    
    // define colours for tetris shapes
    private static let colours : [UIColor] = [
        
        
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0),
        ]
    
    
    //configue settings
    init( settings: TetrisSettings, display: SCNScene, x: Float,  y: Float,  z: Float) {
        self.x = x
        self.y = y
        self.z = z
        
        self.settings = settings
        self.display = display
        
        self.frame = createGridFrame(settings.width, settings.height)
        display.rootNode.addChildNode(self.frame)
    }
    
    //show tetris shape
    func show(_ current: TetrisPosition) {
        recent?.removeFromParentNode()
        recent = SCNNode()
        let tetrisShapes = current.tetrisShape()
        for i in 0...3 {
            recent.addChildNode(block(current, tetrisShapes.x(i), tetrisShapes.y(i)))
        }
        display.rootNode.addChildNode(recent)
    }
    
    // add shape to grid
    func addToGrid(_ current: TetrisPosition) {
        recent?.removeFromParentNode()
        let tetromino = current.tetrisShape()
        for i in 0...3 {
            let box = block(current, tetromino.x(i), tetromino.y(i))
            display.rootNode.addChildNode(box)
            let row = tetromino.y(i) + current.y
            while(blocksByLine.count <= row) {
                blocksByLine.append([])
            }
            blocksByLine[row].append(box)
        }
    }
    
    //remove line when full
    func removeLines(_ lines: [Int], _ scores: Int) -> CFTimeInterval {
        let time = 0.2
        // hide shapes in removed lines
        for line in lines {
            for block in blocksByLine[line] {
                animate(block, "opacity", from: 1, to: 0, during: time)
            }
        }
        Timer.scheduledTimer(withTimeInterval: time, repeats: false) { _ in
            self.showLinesScores(Float(lines.first!), scores)
            
            // lower rest of the tetris shapes
            
            for (index, line) in lines.reversed().enumerated() {
                let nextLine = index + 1 < lines.count ? lines[index + 1] : self.blocksByLine.count
                if (nextLine > line + 1) {
                    for j in line + 1..<nextLine {
                        for block in self.blocksByLine[j] {
                            let y1 = self.y + Float(j) * self.cell - self.cell / 2
                            let y2 = y1 - self.cell * Float(index + 1)
                            self.animate(block, "position.y", from: y1, to: y2, during: time)
                        }
                    }
                }
            }
            // remove lines filled with shapes from the scene
            for line in lines {
                for block in self.blocksByLine[line] {
                    block.removeFromParentNode()
                }
                self.blocksByLine.remove(at: line)
            }
        }
        return time * 2
    }
    
    //drop the shapes when line is full
    func drop(from: TetrisPosition, to: TetrisPosition) -> CFTimeInterval {
        // use animation time from 0.1 to 0.4 seconds depending on distance
        let delta = from.y - to.y
        let percent = Double(delta - 1) / Double(settings.height - 1)
        let time = percent * 0.5 + 0.2
        animate(recent, "position.y", from: 0, to: Float(-delta) * cell, during: time)
        return time
    }
    
    func showGameOver(_ scores: Int) {
        
        // Make gravity effect lower
        display.physicsWorld.gravity = SCNVector3Make(0, -2.0, 0)
        
        // Remove grid frame from the game
        self.frame.removeFromParentNode()
        
        // displays floor node for blocks to fall on
        let matrix = SCNMatrix4Mult(SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0), translate(0, 0))
        let floor = createNode(SCNPlane(width: 10, height: 10), matrix, TetrisDisplay.floorColour)
        floor.physicsBody = SCNPhysicsBody(type: .static, shape: nil)
        display.rootNode.addChildNode(floor)
        
        for i in 0..<blocksByLine.count {
            
            // This will make tetris shapes break up in random directions
            let z = Float((Int(arc4random_uniform(2))))
            let x = Float((Int(arc4random_uniform(2))))
            let direction = SCNVector3Make(x, 0, z)
            for item in blocksByLine[i] {
                item.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
                // use 0.9 angular damping to prevents boxes from rotating like a crazy
                item.physicsBody!.angularDamping = 0.9
                item.physicsBody!.applyForce(direction, asImpulse: true)
            }
        }
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { _ in
            self.showFinalScores(scores)
        }
    }
    
    //creates the tetris grid using height and width from the struct tetris settings
    private func createGridFrame(_ width: Int, _ height: Int) -> SCNNode {
        let node = SCNNode()
        for i in 1...width + 1 {
            addLine(to: node, 0.001, cell * Float(height + 3), 0.001, Float(i), 0, 0)
            addLine(to: node, 0.001, cell * Float(height + 3), 0.001, Float(i), 0, 1)
        }
        for i in 0...height + 3 {
            addLine(to: node, cell * Float(width), 0.001, 0.001, 1, Float(i), 0)
            addLine(to: node, cell * Float(width), 0.001, 0.001, 1, Float(i), 1)
        }
        for i in 1...width + 1 {
            for j in 0...height + 3 {
                addLine(to: node, 0.001, 0.001, cell, Float(i), Float(j), 1)
            }
        }
        return node
    }
    
    //shows score when lines are completed
    private func showLinesScores(_ line: Float, _ scores: Int) {
        let node = createNode(text("+\(scores)"), translate(5, line, 2).scale(0.001), TetrisDisplay.scoresColour)
        let y = node.position.y
        animate(node, "position.y", from: y, to: y + cell * 4, during: 2)
        animate(node, "opacity", from: 1, to: 0, during: 2)
        self.display.rootNode.addChildNode(node)
    }
    
    //shows final score when game is finished
    private func showFinalScores(_ scores: Int) {
        let x = Float(settings.width / 2 - 2)
        let y = Float(settings.height / 2)
        let node = createNode(text("Scores: \(scores)"), translate(x, y).scale(0.003), TetrisDisplay.titleColour)
        self.display.rootNode.addChildNode(node)
    }
    
    // creates nodes
    private func createNode(_ geometry: SCNGeometry, _ matrix: SCNMatrix4, _ color: UIColor) -> SCNNode {
        let material = SCNMaterial()
        material.diffuse.contents = color
        // use the same material for all geometry elements
        geometry.firstMaterial = material
        let node = SCNNode(geometry: geometry)
        node.transform = matrix
        return node
    }
    
    // moves shapes
    private func translate(_ x: Float, _ y: Float, _ z: Float = 0) -> SCNMatrix4 {
        return SCNMatrix4MakeTranslation(self.x + x * cell, self.y + y * cell, self.z + z * cell)
    }
    
    private func cg(_ f: Float) -> CGFloat { return CGFloat(f) }
    
    // creates tetris blocks
    private func block(_ position: TetrisPosition, _ x: Int, _ y: Int) -> SCNNode {
        let cell = cg(self.cell)
        let box = SCNBox(width: cell, height: cell, length: cell, chamferRadius: cell / 10)
        let matrix = translate(Float(position.x + x), Float(position.y + y) - 0.5)
        return createNode(box, matrix, TetrisDisplay.colours[position.index])
    }
    
    // creates the score text
    private func text(_ string: String) -> SCNText {
        let text = SCNText(string: string, extrusionDepth: 2)
        text.font = UIFont.systemFont(ofSize: 30)
        return text
    }
    
    
    // handles animation
    private func animate(_ node: SCNNode, _ path: String, from: Any, to: Any, during: CFTimeInterval) {
        let animation = CABasicAnimation(keyPath: path)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = during
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        node.addAnimation(animation, forKey: nil)
    }
    
    
    // add lines to grid
    private func addLine(to node: SCNNode, _ w: Float, _ h: Float, _ l: Float, _ x: Float, _ y: Float, _ z: Float) {
        let line = SCNBox(width: cg(w), height: cg(h), length: cg(l), chamferRadius: 0)
        let matrix = SCNMatrix4Translate(translate(x - 0.5, y, z - 0.5), w / 2, h / 2, -l / 2)
        node.addChildNode(createNode(line, matrix, TetrisDisplay.gridColour))
    }
    
    
    
    
}

extension SCNMatrix4 {
    func scale(_ s: Float) -> SCNMatrix4 { return SCNMatrix4Scale(self, s, s, s) }
}

